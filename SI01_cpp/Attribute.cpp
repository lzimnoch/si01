#include "Attribute.h"

Attribute::Attribute() {}

Attribute::Attribute(int t_val) : m_val(t_val) {}

Attribute::Attribute(int t_val, bool t_decision) : m_val(t_val), m_decision(t_decision) {}

Attribute::~Attribute() {}

int Attribute::GetVal()
{
	return m_val;
}

bool Attribute::operator ==(Attribute t_attr)
{
	if (this->m_val == t_attr.m_val)
		return true;
	return false;
}

bool Attribute::operator !=(Attribute t_attr)
{
	if (this->m_val != t_attr.m_val)
		return true;
	return false;
}