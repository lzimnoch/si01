#pragma once

#include <iostream>

class Attribute
{
public:
	Attribute();
	Attribute(int t_val);
	Attribute(int t_val, bool t_decision); //rather useless
	~Attribute();

	int GetVal();

	bool operator ==(Attribute t_attr);
	bool operator !=(Attribute t_attr);

private:
	int m_val;
	bool m_decision;
};
