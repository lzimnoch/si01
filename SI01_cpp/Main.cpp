#include <iostream>
#include <algorithm> //permutation
#include <string>
#include <stdlib.h>
#include <vector> 
#include <numeric> //iota
#include <fstream>

#include "Observation.h"
#include "Attribute.h"
#include "Matrix.h"




std::vector<std::vector<int>> ReadFile(std::string t_fileName)
{
	std::vector<std::vector<int>> readFile;
	std::ifstream myFile;
	myFile.open(t_fileName, std::ios::out);
	std::string line;
	int rowCount = 0;
	std::vector<std::vector<int>> returnedVector;

	if (myFile.is_open())
	{
		//std::cout << "ReadFile: " << std::endl;
		while (std::getline(myFile, line))
		{
			std::vector<int> myLine;
			for (int j = 0; j < line.size(); j++)
			{
				if (j % 2 == 0)
				{
					myLine.insert(myLine.end(), line[j] - '0');
					//std::cout << (int)myLine[j/2] << " ";
				}
			}
			rowCount++;
			readFile.insert(readFile.end(), myLine);
			//std::cout << std::endl;
		}
		
	}
	else
	{
		std::cout << "Brak pliku!" << std::endl;
		system("PAUSE");
		exit(1);
	}
	//std::cout << ">>>>>>>>>>>>>>>>>>" << std::endl;

	/*
	for (int i = 0; i < readFile.size(); i++)
	{
		for (int j = 0; j < readFile[0].size(); j++)
		{
			std::cout << readFile[i][j] << " ";
		}
		std::cout << std::endl;
	}*/
	return readFile;
}


int main()
{
	//std::vector<std::vector<int>> readFileVector = ReadFile("SystemDecyzyjny.txt");
	Matrix coveringMatrix = Matrix(ReadFile("SystemDecyzyjny.txt"));
	Matrix exhaustiveMatrix = coveringMatrix;

	std::cout << "Covering: " << std::endl;
	coveringMatrix.RunCoveringRules();

	std::cout << std::endl << "Exhaustive: " << std::endl;

	exhaustiveMatrix.RunExhaustive();


	std::cout << std::endl;
	system("PAUSE");

	return 0;
}