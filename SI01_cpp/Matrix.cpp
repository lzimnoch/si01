#include "Matrix.h"


Matrix::Matrix(std::vector<Observation*> t_observations) : m_observationsList(t_observations) {}

Matrix::Matrix(std::vector<std::vector<int>> t_readFile)
{
	for (int i = 0; i < t_readFile.size(); i++)
	{
		m_observationsList.push_back(new Observation());
		for (int j = 0; j < t_readFile[0].size(); j++)
		{
			m_observationsList[i]->setObsId(i + 1);
			if (j == t_readFile[0].size() - 1)
			{
				m_observationsList[i]->SetDecision(new Attribute(t_readFile[i][j]));
				break;
			}
			m_observationsList[i]->AddAttribute(new Attribute(t_readFile[i][j]));
		}
	}
}

Matrix::~Matrix() {}

std::vector<std::vector<std::vector<int>>> Matrix::CountDiffMatrix()
{
	int observationsCount = m_observationsList.size();

	std::vector<std::vector<std::vector<int>>> returnedValue;

	returnedValue = std::vector<std::vector<std::vector<int>>>(observationsCount, std::vector<std::vector<int>>(observationsCount));

	for (int j = 0; j < observationsCount; j++)
	{
		for (int i = 0; i < observationsCount; i++) //dla uproszczenia (Ogromnego!), zamiast traingular matrix zrobie kwadratowa
		{
			Observation *compareRow1 = m_observationsList[i];
			Observation *compareRow2 = m_observationsList[j];

			returnedValue[i][j] = compareRow1->GetCollidingAttributes(compareRow2);
		}
	}
	return returnedValue;
}

//prawie taki sam kod jest w observation.cpp, jak starczy czasu to fajnie by�oby wyrzuci� cz�� wsp�ln� do funkcji gdzie�
/*bool CheckIfDiffCellColliding(std::vector<int> t_diffCell, std::vector<int> t_permutations)
{
	std::vector<int> returnedValue;

	returnedValue.insert(returnedValue.begin(), t_permutations.begin(), t_permutations.end());
	returnedValue.insert(returnedValue.end(), t_diffCell.begin(), t_diffCell.end());

	sort(returnedValue.begin(), returnedValue.end());
	returnedValue.erase(std::unique(returnedValue.begin(), returnedValue.end()), returnedValue.end());

	if (returnedValue.size() == t_diffCell.size())
		return true;
	return false;
}*/

bool Matrix::IsRowCovered(std::vector<int> t_mainRow, std::vector<int> t_comparedRow)
{
	std::vector<int> returnedValue;

	returnedValue.insert(returnedValue.begin(), t_mainRow.begin(), t_mainRow.end());
	returnedValue.insert(returnedValue.end(), t_comparedRow.begin(), t_comparedRow.end());

	sort(returnedValue.begin(), returnedValue.end());
	returnedValue.erase(std::unique(returnedValue.begin(), returnedValue.end()), returnedValue.end());

	if (returnedValue.size() == t_mainRow.size())
		return true;
	return false;
}

bool Matrix::checkIfDiffRowColliding(std::vector<std::vector<int>> t_diffRow, std::vector<int> t_permutations)
{
	for (int i = 0; i < t_diffRow.size(); i++)
	{
		if (IsRowCovered(t_diffRow[i], t_permutations))
			return true;
	}
	return false;
}

/*std::vector<std::pair<Observation*, int>> Matrix::CountCoveringRules(std::vector<Observation*> t_rules)
{
	std::vector<std::pair<Observation*, int>> returnedVector;
	for (int obsCounter1 = 0; ; obsCounter1++)
	{
		if (obsCounter1 >= t_rules.size()) break;
		returnedVector.push_back(std::pair<Observation*, int>(t_rules[obsCounter1], 1));

		for (int obsCounter2 = obsCounter1+1; ; obsCounter2++)
		{
			if (obsCounter2 >= t_rules.size()) break;
			if ((*t_rules[obsCounter1]).EqualAttributes((*t_rules[obsCounter2])))
			{
				returnedVector[returnedVector.size() - 1].second++;
				t_rules.erase(t_rules.begin() + obsCounter2);
			}
		}
	}
	return returnedVector;
}*/

bool compareIntVectors(std::vector<int> t_v1, std::vector<int> t_v2)
{
	for (int i = 0; i < t_v1.size(); i++)
	{
		if (t_v1[i] != t_v2[i])
			return false;
	}
	return true;
}

std::vector<std::pair<std::vector<int>, int>> Matrix::CountCoveringRules(std::vector<std::vector<int>> t_rules)
{
	std::vector<std::pair<std::vector<int>, int>> returnedValue;
	for (int rulesCounter1 = 0; ; rulesCounter1++)
	{
		if (rulesCounter1 >= t_rules.size()) break;
		returnedValue.push_back(std::pair<std::vector<int>, int>(t_rules[rulesCounter1], 1));

		for (int rulesCounter2 = rulesCounter1 + 1; ; rulesCounter2++)
		{
			if (rulesCounter2 >= t_rules.size() || rulesCounter1 >= t_rules.size()) break;

			if (t_rules[rulesCounter1].size() == t_rules[rulesCounter2].size())
			{
				//if (IsRowCovered(t_rules[rulesCounter1], t_rules[rulesCounter2]))
				//if(t_rules[rulesCounter1] == t_rules[rulesCounter2]) //to tez jakby dziala, do podmienienia
				if(compareIntVectors(t_rules[rulesCounter1], t_rules[rulesCounter2]))
				{
					//co� gdzie� w tych okolicach �mierdzi b��dem
					returnedValue[returnedValue.size() - 1].second++;
					t_rules.erase(t_rules.begin() + rulesCounter2);
					rulesCounter2 = 0;
				}
			}
		}
	}
	return returnedValue;
}

void Matrix::RunExhaustive()
{
	std::vector<std::vector<std::vector<int>>> diffMatrix = CountDiffMatrix();

	int observationsCount = m_observationsList.size();
	int attributeCount = m_observationsList[0]->getSize();

	std::vector<std::vector<int>> permutations;

	std::vector<std::vector<int>> rules;

	for (int i = 1; i < attributeCount; i++)
	{
		std::vector<std::vector<int>> permutations = Combination(attributeCount, i);
		for (int j = 0; j < observationsCount; j++)
		{
			for (int k = 0; k < permutations.size(); k++)
			{
				if (!checkIfDiffRowColliding(diffMatrix[j], permutations[k]))
				{
					if ((*m_observationsList[j]).addExhaustiveRule(permutations[k]))
					{
						std::vector<int> tempRule;
						for (int permCounter = 0; permCounter < permutations[0].size(); permCounter++)
						{
							tempRule.push_back(permutations[k][permCounter]+1);
							tempRule.push_back((*m_observationsList[j])[permutations[k][permCounter]]->GetVal());
						}
						tempRule.push_back((*m_observationsList[j]).GetDecision()->GetVal());

						rules.push_back(tempRule);
						//zamiast wypisywa�, zapisz� do vector<pair<vector, attr>>, a p�niej b�d� szuka�
						//pokrywaj�cych si� decyzji

						/*std::cout << std::endl << "o" << m_observationsList[j]->getObsId() << ": ";
						for (int z = 0; z < permutations[0].size(); z++)
						{
							std::cout << "(a" << permutations[k][z] + 1 << " = " << (*m_observationsList[j])[permutations[k][z]]->GetVal() << ")";
						}
						std::cout << "=> (d = " << (*m_observationsList[j]).GetDecision()->GetVal() << ")";

						//dodanie [X], pokrywanie regu�
						//to ni�ej do wywalenia, mam lepszy pomys�
						int coveringCounter = 1;

						bool isCovering;
						for (int obsCounter = 0; obsCounter < observationsCount; obsCounter++)
						{
							isCovering = true;
							if ((*m_observationsList[obsCounter]).GetDecision()->GetVal() != (*m_observationsList[j]).GetDecision()->GetVal() || obsCounter == j)
								continue;

							for (int permCounter = 0; permCounter < permutations[0].size(); permCounter++)
							{
								if ((*m_observationsList[obsCounter])[permutations[k][permCounter]] != (*m_observationsList[j])[permutations[k][permCounter]])
								{
									isCovering = false;
									break;
								}
							}
							if (isCovering)
								coveringCounter++;
						}
						if (coveringCounter > 1)
							std::cout << "[" << coveringCounter << "]";*/
					}
				}
			}		
		}
	}

	std::vector<std::pair<std::vector<int>, int>> rulesToPrint = CountCoveringRules(rules);

	for (int rulesCount = 0; rulesCount < rulesToPrint.size(); rulesCount++)
	{
		for (int attrCount = 0; attrCount+1 < rulesToPrint[rulesCount].first.size(); attrCount++)
		{
			std::cout << "(a" << rulesToPrint[rulesCount].first[attrCount] << " = " << rulesToPrint[rulesCount].first[++attrCount] << ")";
		}
		std::cout << " => (d = " << rulesToPrint[rulesCount].first[rulesToPrint[rulesCount].first.size()-1] << ")" << std::endl;
	}
	/*std::vector<std::pair<Observation*, int>> rulesToPrint = CountCoveringRules(rules);
	//tutaj wypisywanie
	for (int rulesCount = 0; rulesCount < rulesToPrint.size(); rulesCount++)
	{
		int ruleDecision = (*rulesToPrint[rulesCount].first).GetDecision()->GetVal();
		
		int obsId = (*rulesToPrint[rulesCount].first).getObsId();

		for (int attrCount = 0; attrCount < rulesToPrint[0].first->getSize(); attrCount++)
		{
			int obsIndexCounter = (*rulesToPrint[rulesCount].first)[attrCount]->GetVal();
			std::cout << "(a" << obsIndexCounter << " = " << (*m_observationsList[obsId])[obsIndexCounter]->GetVal() << ")";
		}
		std::cout << " => (d = " << (*m_observationsList[obsId]).GetDecision()->GetVal() << ")" << std::endl;
	}*/

	
}


//looks like I have to run this command for every next row
int Matrix::FindCoveringRules(int t_consideredRow, std::vector<std::vector<int>> t_permutations)
{
	int observationCount = m_observationsList.size();
	int attrCount = m_observationsList[0]->getSize();
	std::vector<Observation*> differentDecision; //different decision than t_consideredRow, just in case if there's more than just binary d
	std::vector<Observation*> sameDecision;
	Observation *consideredRow = m_observationsList[t_consideredRow];

	for (int i = 0; i < observationCount; i++)
	{
		if ((*m_observationsList[t_consideredRow]).GetDecision()->GetVal() != (*m_observationsList[i]).GetDecision()->GetVal())
			differentDecision.push_back(m_observationsList[i]);
		else if (i != t_consideredRow) //exclude considered row, to avoid stupid errors
			sameDecision.push_back(m_observationsList[i]);
	}

	int differentDecisionCount = differentDecision.size();
	int sameDecisionCount = sameDecision.size();
	bool equals = false;
	bool diffCollision;
	int diffCounter;
	bool sameDifference;
	for (int i = 0; i < t_permutations.size(); i++)
	{
		diffCollision = false;
		for (int k = 0; k < differentDecisionCount; k++)
		{
			diffCounter = 0;
			for (int j = 0; j < t_permutations[0].size(); j++)
			{
				if ((*(*consideredRow)[t_permutations[i][j]]) == (*(*differentDecision[k])[t_permutations[i][j]]))
				{
					diffCounter++;
					if(diffCounter == t_permutations[0].size())
						diffCollision = true;
				}
				if (diffCollision) break;
			}
			if (diffCollision) break;
		}
		if (!diffCollision) 
		{
			consideredRow->setAsUsed();

			std::cout << std::endl << "o" << consideredRow->getObsId() << ": "; //o1: 
				
				
			std::cout << "(a" << t_permutations[i][0] + 1 << " = " << (*consideredRow)[t_permutations[i][0]]->GetVal() << ")";
			for (int t = 1; t < t_permutations[0].size(); t++)
			{
				std::cout << " & (a" << t_permutations[i][t] + 1 << " = " << (*consideredRow)[t_permutations[i][t]]->GetVal();
			}
					
					
			std::cout << ") => (d = " << (*consideredRow).GetDecision()->GetVal() << ")"; //) => (d = 1)
			
			int supportCounter = 1;

			for (int s = 0; s < sameDecisionCount; s++)
			{
				sameDifference = false;
				for (int j = 0; j < t_permutations[0].size(); j++)
				{
					if ((*(*consideredRow)[t_permutations[i][j]]) != (*(*sameDecision[s])[t_permutations[i][j]]))
					{
						sameDifference = true;
						break;
					}
				}

				if (!sameDifference)
					supportCounter++;
			}
			if (supportCounter > 1)
				std::cout << "[" << supportCounter << "]";

			//consideredRow[t_permutations[i][j] is a rule, check for the same rules and throw them away
			for (int s = 0; s < sameDecisionCount; s++)
			{
				sameDifference = false;
				for (int j = 0; j < t_permutations[0].size(); j++)
				{
					if ((*(*consideredRow)[t_permutations[i][j]]) != (*(*sameDecision[s])[t_permutations[i][j]]))
					{
						sameDifference = true;
						break;
					}
				}
				
				if (!sameDifference && !sameDecision[s]->getUsed())
				{
					//wyrzucenie obiektu sameDecision[s] z rozwazan, obserwacja przykryta decyzj�
					std::cout << std::endl << "o" << sameDecision[s]->getObsId() << " wyrzucona z rozwazan. Pokryta przez powyzsze decyzje";
					sameDecision[s]->setAsUsed();
				}
			}
			break;
		}
	}

	return int();
}

void Matrix::RunCoveringRules()
{
	for (int j = 1; j < m_observationsList[0]->getSize(); j++)
	{
		for (int i = 0; i < m_observationsList.size(); i++)
		{
			if (!m_observationsList[i]->getUsed())
				FindCoveringRules(i, Combination(m_observationsList[i]->getSize(), j));
		}
		bool exit = true;
		for (int i = 0; i < m_observationsList.size(); i++)
		{
			if (!m_observationsList[i]->getUsed())
				exit = false;
		}
		if (exit)
			break;
	}
	std::cout << std::endl << "====================" << std::endl;
}

//Silnia
int Matrix::Factorial(int t_num)
{
	int returned = 1;
	for (int i = 1; i <= t_num; i++)
	{
		returned *= i;
	}
	return returned;
}

std::vector<std::vector<int>> Matrix::Combination(int t_N, int t_K)
{   //N po K   |-o=o-|-o=o-|   (N / K)
	std::string bitmask(t_K, 1); // K leading 1's
	bitmask.resize(t_N, 0); // N-K trailing 0's

	int newtonPermCount = Factorial(t_N) / ((Factorial(t_K) * Factorial(t_N - t_K)));

	std::vector<std::vector<int>> returnedVector(newtonPermCount, std::vector<int>(t_K));
	int rowcount = 0, colCount;
	do {
		colCount = 0;
		for (int i = 0; i < t_N; ++i) // [0..N-1] integers
		{
			if (bitmask[i])
			{
				returnedVector[rowcount][colCount] = i;
				colCount++;
			}
			//if (bitmask[i]) std::cout << " " << i;
		}
		//std::cout << std::endl;
		rowcount++;
	} while (std::prev_permutation(bitmask.begin(), bitmask.end()));
	return returnedVector;
}