#pragma once

#include <vector>
#include <algorithm>

#include "Observation.h"


class Matrix
{
public:
	Matrix(std::vector<Observation*> t_observations);
	Matrix(std::vector<std::vector<int>>);
	~Matrix();

	std::vector<std::vector<std::vector<int>>> CountDiffMatrix();
	void RunExhaustive();

	static bool IsRowCovered(std::vector<int> t_mainRow, std::vector<int> t_comparedRow);
	int FindCoveringRules(int, std::vector<std::vector<int>>);

	bool checkIfDiffRowColliding(std::vector<std::vector<int>> t_diffRow, std::vector<int> t_permutations);
	//bool Matrix::CheckDiff(std::vector<int> t_diff, std::vector<int> t_perm);

	//std::vector<std::pair<Observation*, int>> Matrix::CountCoveringRules(std::vector<Observation*> t_rules);
	std::vector<std::pair<std::vector<int>, int>> Matrix::CountCoveringRules(std::vector<std::vector<int>> t_rules);

	void RunCoveringRules();


	int Factorial(int t_num);
	std::vector<std::vector<int>> Combination(int t_N, int t_K);

private:
	std::vector<Observation*> m_observationsList;
};
