#include "Observation.h"
#include "Matrix.h"

#include <iostream>
#include <algorithm>



Observation::Observation()
{
	m_attrVector = *(new std::pair< std::vector<Attribute*>, Attribute*>);
}

Observation::Observation(std::vector<int> t_attrVals)
{
	int vectorSize = t_attrVals.size();
	for (int i = 0;; i++)
	{
		if (i == vectorSize - 1)
		{
			m_attrVector.second = &Attribute(t_attrVals[i]);
			break;
		}
		m_attrVector.first.push_back(&Attribute(t_attrVals[i]));
	}
}

Observation::Observation(std::vector<int> t_attrVals, int t_obsNo, bool t_decision)
{
	this->setObsId(t_obsNo);
	int vectorSize = t_attrVals.size();
	for (int i = 0; i < vectorSize; i++)
	{
		if (i == vectorSize - 1 && t_decision)
		{
			m_attrVector.second = &Attribute(t_attrVals[i]);
			break;
		}
		m_attrVector.first.push_back(&Attribute(t_attrVals[i]));
	}
}

Observation::Observation(std::vector<Attribute*> t_attrVector, int t_obsNo)
{
	this->setObsId(t_obsNo);
	int vectorSize = t_attrVector.size();
	for (int i = 0;; i++)
	{
		if (i == vectorSize - 1)
		{
			m_attrVector.second = t_attrVector[i];
			break;
		}
		m_attrVector.first.push_back(t_attrVector[i]);
	}
}

Observation::~Observation() {}

void Observation::AddAttribute(Attribute *t_attr)
{
	m_attrVector.first.push_back(t_attr);
}

void Observation::SetDecision(Attribute *t_attr)
{
	m_attrVector.second = t_attr;
}

Attribute* Observation::GetDecision()
{
	return m_attrVector.second;
}

std::vector<Attribute*> Observation::GetAttributes()
{
	return m_attrVector.first;
}

std::vector<int> Observation::GetCollidingAttributes(Observation *t_observation)
{
	std::vector<int> returnedValue;
	int attributesCount = getSize();
	if (attributesCount != t_observation->getSize())
		return returnedValue;

	//sprawdzenie decyzji
	if ((*GetDecision()) == (*t_observation->GetDecision()))
		return returnedValue;

	for (int i = 0; i < attributesCount; i++)
	{
	if ((*(GetAttributes())[i]) == (*(t_observation->GetAttributes())[i]))
		returnedValue.push_back(i);
	}
	
	return returnedValue;
}

bool Observation::addExhaustiveRule(std::vector<int> t_attrIndex)
{
	std::vector<int> returnedValue;
	for (int i = 0; i < m_rules.size(); i++)
	{
		if (Matrix::IsRowCovered(t_attrIndex, m_rules[i]))
			return false;
	}
	
	m_rules.push_back(t_attrIndex);
	return true;
}

bool Observation::EqualAttributes(Observation t_observation)
{
	if (getSize() != t_observation.getSize())
		return false;
	for (int attrCounter = 0; attrCounter < getSize(); attrCounter++)
	{
		if ((*this)[attrCounter]->GetVal() != t_observation[attrCounter]->GetVal())
			return false;
	}
	return true;
}

Attribute* Observation::operator[](int t_attrIndex)
{
	return m_attrVector.first[t_attrIndex];
}

bool Observation::operator ==(Observation t_observation)
{
	if (getSize() != t_observation.getSize() || GetDecision() != t_observation.GetDecision())
		return false;

	for (int attrCounter = 0; attrCounter < getSize(); attrCounter++)
	{
		if ((*this)[attrCounter]->GetVal() != t_observation[attrCounter]->GetVal())
			return false;
	}
	return true;
}

bool Observation::operator !=(Observation t_observation)
{
	if (getSize() != t_observation.getSize() || GetDecision() != t_observation.GetDecision())
		return true;

	for (int attrCounter = 0; attrCounter < getSize(); attrCounter++)
	{
		if ((*this)[attrCounter]->GetVal() != t_observation[attrCounter]->GetVal())
			return true;
	}
	return false;
}

int Observation::getSize()
{
	return GetAttributes().size();
}
