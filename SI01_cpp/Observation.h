#pragma once

#include <vector>

#include "Attribute.h"


class Observation
{
public:
	Observation();
	Observation(std::vector<Attribute*>, int);
	Observation::Observation(std::vector<int>);
	Observation::Observation(std::vector<int>, int, bool);
	~Observation();

	void Observation::SetDecision(Attribute *t_attr);
	Attribute* Observation::GetDecision();
	void AddAttribute(Attribute *t_attr);
	std::vector<Attribute*> GetAttributes();
	std::vector<int> GetCollidingAttributes(Observation *t_observation);
	int getSize();
	int getObsId() { return m_obsId; }

	bool getUsed() { return m_used; }
	void setAsUsed() { m_used = true; }
	void setObsId(int t_obsId) { m_obsId = t_obsId; }

	bool addExhaustiveRule(std::vector<int> t_attrIndex);

	bool Observation::EqualAttributes(Observation);

	Attribute* Observation::operator[](int t_attrIndex);
	bool Observation::operator ==(Observation t_observation);
	bool Observation::operator !=(Observation t_observation);
	

private:

	int m_obsId;
	bool m_used = false; //if the decision is made
	
	std::vector<std::vector<int>> m_rules;

	//std::vector<Attribute*> m_attrVector;
	std::pair < std::vector<Attribute*>, Attribute*> m_attrVector;
};
